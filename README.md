# GFP

GFP (green filtering of packets) is a tool used to capture specific system calls made by docker containers thereby capturing the activity within containers.

The naming was inspired by gfp used in molecular biology, green fluroescent protein, which is a laboratory technique to tag proteins and capture molecular and cellular data via microscopy. Similar to this technique, this tool uses ebpf (extended berkeley packet filters) to capture the kernel system calls made by containers and return that information back to the developer.


## Architecture
```
+-----------+           +-----------+           +-----------+
|  ebpf     | kafka     |  indexer  |           |           |
|  service  |---------->|  service  |---------->|    es     |
|    (go)   |           |    (go)   |           |           |
+-----------+           +-----^-----+           +-----------+
                              |kafka                  |
                        +-----+-----+                 |
                        |    log    |                 |
                        |  sorter   |   reads         |
                        |   (go)    | <----------------
                        +-----------+
```

The calls will be logged to elasticsearch via kafka and a go microservice. The log sorter service will read the logs and group them by containers. This service will then rewrite these logs to a structure index in es (by container name).

e.g. processed syscall trace of a container

```
{
	"container_name": "test",
	"timestamp": "2020-01-01",
	"syscalls_pattern": "/usr/bin/ls,/usr/bin/cat /usr/share/test.txt"
}
```


# Usage

```
make
```

# Technologies used

+ ebpf
+ golang
+ elasticsearch
+ kafka

# Development

## Dependencies

+ recent version of linux kernel
+ docker
+ docker-compose
